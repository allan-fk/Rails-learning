def my_var
  tab = {
    'a' => 10,
    'b' => '10',
    'c' => "dix",
    'd' => 10.0,
  }
  puts 'mes variables :'
  tab.each do |key, value|
    puts "\t #{key} contiens : #{value} et est de type: #{value.class}"
  end
end
my_var
